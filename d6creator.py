import sys

import os.path
import xml.dom.minidom as mini

depositedCountry = "US"
year = "2020"
name = "MR POTATO"
lastName = "HEAD"
dni  = "69696969X"
tipoVia = "CL"
nombreVia = "LA PIRULETA"
numeroVia = "1"
piso = "1"
puerta = "A"
cp = "99999"
municipio = "MUNICIPIO"
provincia = "MUNICIPIO"
fecha = "04/01/2020"
telefono = "696969669"
correo = "mrpotato@gmail.com"

numPagina       = [  1  ,  1  ,  1  ,  2  ,  2  ,  2  ,  2  ,  2  ,  2  ]
codigo10por     = ["2E8","2F4","300","326","332","33E","34A","356","362"]
codigoISIN      = ["2E9","2F5","301","327","333","33F","34B","357","363"]
codigoDescrip   = ["2EA","2F6","302","328","334","340","34C","358","364"]
codigoEmisor    = ["2EB","2F7","303","329","335","341","34D","359","365"]
codigoValor     = ["2EC","2F8","304","32A","336","342","34E","35A","366"]
codigoPais      = ["2ED","2F9","305","32B","337","343","34F","35B","367"]
codigoMoneda    = ["2EE","2FA","306","32C","338","344","350","35C","368"]
codigoNacciones = ["2EF","2FB","307","32D","339","345","351","35D","369"]
codigoEfectivo  = ["2F1","2FD","309","32F","33B","347","353","35F","36B"]

codigoTipoDeposito  = ["2DB","320"]
codigoAno           = ["2DC","321"]
codigoNombre        = ["2DF","322"]
codigoDNI           = ["2E0","323"]
codigoTipoVia       = ["2E1"]
codigoNombreVia     = ["2E2"]
codigoNumeroVia     = ["2E3"]
codigoPostal        = ["2E4"]
codigoMunicipio     = ["2E5"]


def checkAttributes(doc):
	openPosition = doc.getElementsByTagName('OpenPosition')

	for position in openPosition:
		for attribute in position.attributes.items():
			if not attribute[1]:
				print "Atributo vacio"
				return False

	return True;

def saveToFile(root):
	saveFileName = "result.aforixm"

	with open(saveFileName, "w") as f:
		f.write(root.toprettyxml())


def insertField(fieldCode,dataValue):
	root = mini.Document()
	# Insert deposit statement field
	fieldElement = root.createElement('Campo')

	codeElement = root.createElement('Codigo')
	codeContent = root.createTextNode(fieldCode)
	codeElement.appendChild(codeContent)
	fieldElement.appendChild(codeElement)

	dataElement = root.createElement('Datos')
	dataContent = root.createTextNode(dataValue)
	dataElement.appendChild(dataContent)
	fieldElement.appendChild(dataElement)

	return fieldElement


def createAforixXML():
	root = mini.Document()
	#aforix = root.createElement('AFORIX')
	#root.appendChild(aforix)
	formulario = root.createElement('Formulario')
	#formulario.setAttribute('id','Formulario')
	root.appendChild(formulario)

	tipoFormularioElement = root.createElement('Tipo')
	tipoFormularioContent = root.createTextNode('D-6')
	tipoFormularioElement.appendChild(tipoFormularioContent)
	formulario.appendChild(tipoFormularioElement)

	versionElement = root.createElement('Version')
	versionContent = root.createTextNode('R10')
	versionElement.appendChild(versionContent)
	formulario.appendChild(versionElement)

	# VersionAFORIXElement = root.createElement('VersionAFORIX')
	# VersionAFORIXContent = root.createTextNode('C81_G')
	# VersionAFORIXElement.appendChild(VersionAFORIXContent)
	# formulario.appendChild(VersionAFORIXElement)

	# VersionJavaElement = root.createElement('VersionJava')
	# VersionJavaContent = root.createTextNode('1.8.0_222')
	# VersionJavaElement.appendChild(VersionJavaContent)
	# formulario.appendChild(VersionJavaElement)

	# First page
	paginaElement = root.createElement('Pagina')
	formulario.appendChild(paginaElement)

	tipoElement = root.createElement('Tipo')
	tipoContent = root.createTextNode('D61')
	tipoElement.appendChild(tipoContent)
	paginaElement.appendChild(tipoElement)

	camposElement = root.createElement('Campos')
	paginaElement.appendChild(camposElement)

	# Insert deposit statement field
	camposElement.appendChild( insertField(codigoTipoDeposito[0],"D") )

	# Insert year field
	camposElement.appendChild( insertField(codigoAno[0],year) )

	# Insert name field
	camposElement.appendChild( insertField(codigoNombre[0],name + " " + lastName) )

	# Insert DNI field
	camposElement.appendChild( insertField(codigoDNI[0],dni) )

	# Insert type of address field
	camposElement.appendChild( insertField(codigoTipoVia[0],tipoVia) )

	# Insert street name field
	camposElement.appendChild( insertField(codigoNombreVia[0],nombreVia) )

	# Insert number of hallway field
	camposElement.appendChild( insertField(codigoNumeroVia[0],numeroVia) )

	# Insert postal number field
	camposElement.appendChild( insertField(codigoPostal[0],cp) )

	# Insert council field
	camposElement.appendChild( insertField(codigoMunicipio[0],municipio) )

	# Diligencias Declaracion
	camposElement.appendChild( insertField("30A",name + " " + lastName) )
	camposElement.appendChild( insertField("30B",municipio) )
	camposElement.appendChild( insertField("30C",nombreVia) )
	camposElement.appendChild( insertField("30D",numeroVia) )
	camposElement.appendChild( insertField("30E",dni) )
	camposElement.appendChild( insertField("310",provincia) )
	camposElement.appendChild( insertField("311",fecha) )

	# Diligencias datos de contacto
	camposElement.appendChild( insertField("312",name + " " + lastName) )
	camposElement.appendChild( insertField("053",tipoVia + " " + nombreVia + " " + numeroVia) )
	camposElement.appendChild( insertField("063",(cp + " " + municipio)[0:30]) )
	camposElement.appendChild( insertField("064",provincia) )
	camposElement.appendChild( insertField("319",telefono) )
	camposElement.appendChild( insertField("066",correo) )


	openPosition = doc.getElementsByTagName('OpenPosition')


	index = 0
	page = 1
	newPage = False

	print "Total: " + str(len(openPosition))
	print "Page: " + str(page)

	for position in openPosition:

		if (page == 1 and index > 2):
			page = page + 1
			newPage = True

		if index > 8:
			index = 3
			page = page + 1
			newPage = True


		if newPage == True:
			newPage = False
			#page = numPagina[index]
			paginaElement = root.createElement('Pagina')
			formulario.appendChild(paginaElement)

			tipoElement = root.createElement('Tipo')
			tipoContent = root.createTextNode('D62')
			tipoElement.appendChild(tipoContent)
			paginaElement.appendChild(tipoElement)

			camposElement = root.createElement('Campos')
			paginaElement.appendChild(camposElement)

			# Insert deposit statement field
			camposElement.appendChild( insertField(codigoTipoDeposito[1],"D") )

			# Insert year field
			camposElement.appendChild( insertField(codigoAno[1],year) )


			print "Page: " + str(page)

		print "Index: " + str(index) + " " + str(len(numPagina))

		# Participacion superior al 10%
		camposElement.appendChild( insertField(codigo10por[index],"N") )


		# Pais
		camposElement.appendChild( insertField(codigoPais[index],depositedCountry) )

		# Emisor
		if position.getAttribute("isin").find("ES") != -1:
			camposElement.appendChild( insertField(codigoEmisor[index],"800") )
		else:
			camposElement.appendChild( insertField(codigoEmisor[index],"400") )

		# Valor
		camposElement.appendChild( insertField(codigoValor[index],"01") )


		for attribute in position.attributes.items():
			codigo = ""
			dato   = ""
			if (attribute[0] == "isin"):
				codigo = codigoISIN[index]
				dato   = position.getAttribute("isin")
			if (attribute[0] == "description"):
				codigo = codigoDescrip[index]
				dato   = position.getAttribute("description")
			if (attribute[0] == "currency"):
				codigo = codigoMoneda[index]
				dato   = position.getAttribute("currency")
			if (attribute[0] == "position"):
				codigo = codigoNacciones[index]
				dato   = position.getAttribute("position")
			if (attribute[0] == "positionValue"):
				codigo = codigoEfectivo[index]
				dato   = position.getAttribute("positionValue")
				dato   = dato.replace(".",",")


			if (codigo and dato):
				camposElement.appendChild( insertField(codigo,dato) )




		index = index + 1

	return root


# ------------- MAIN --------------------------


if (len(sys.argv) == 2):

	file = sys.argv[1];

	if (os.path.exists(file) == True):

		print "Reading file " + file

		doc = mini.parse(file);

		if checkAttributes(doc) == True:
			print "All attributes are filled"
			root = createAforixXML()
			#print root.toprettyxml()
			saveToFile(root)

		else:
			print "Something was wrong with your file"
				

else:
	print "Insert file.xml as argument."


